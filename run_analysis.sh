#!/usr/bin/env bash
#
# run_analysis.sh
# 
# Copyright 2015 Jacob Merson <jacob.merson@gmail.com> 
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

TIME=$(date +"%x %r %Z")
#time after which to kill the simulation
#in hours
declare -i SIMULATION_DURATION=1
#convert duration to seconds
SIMULATION_DURATION=$SIMULATION_DURATION*3600
EMAIL="$USER"
MESSAGE="message.txt"
declare -i TERMINATED=0

function kill_job() {
    echo "Terminating $1 at $TIME"
    abaqus terminate job="$1"
    SUBJECT="Job $1 terminated at $TIME"
    echo "Job $1 terminated at $TIME" >> $MESSAGE
}

for f in $@
do
    TIME=$(date +"%x %r %Z")
    echo "Starting job=$f at $TIME"
    echo "Starting job=$f at $TIME" > $MESSAGE
    #set trap to cleanly take care of shutting abaqus down
    trap "kill_job $f" SIGINT
    abaqus job="$f" ask_delete=off
    #sleep for 20 sec to allow abaqus to start
    sleep 20
    #while the lock file exists (simulation running)
    #sleep for 5 sec at a time
    while [ -f ${f}.lck ]; do
        sleep 5
        SIMULATION_DURATION=$SIMULATION_DURATION-5
        #if the simulation has been running for more than the specified time
        #Cleanly terminate the job
        if [ $SIMULATION_DURATION -lt 0 ]
            then
		TIME=$(date +"%x %r %Z")
                kill_job "$f"
                TERMINATED=1
                break
        fi
    done
    if [ $TERMINATED -eq 0 ]
    then
        TIME=$(date +"%x %r %Z")
        SUBJECT="Job $f Completed at $TIME"
        echo "Job $f Completed at $TIME" >> $MESSAGE
    fi
    mail -a "$f.dat" -a "$f.sta" -s "$SUBJECT" "$EMAIL" < $MESSAGE
done

rm "message.txt"
