#!/usr/bin/env python
#
# clean_abaqus.py
# 
# Copyright 2015 Jacob Merson <jacob.merson@gmail.com> 
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import glob
import os

##############
# This file removes all of the standard abaqus files from
# the current directory
##############

#specify files to delete with glob syntax
file_types = ['*.sta', '*.dat', '*.log','*.rpy','*.rpy.*', '*.msg', '*.dat',
              '*.com', '*.sim','*.log','*.msg', '*.prt', '*.023', '*.ipm',
              '*.mdl', '*.stt', '*.rec', '*.aif']


def delete_files(files):
    for f in files:
        os.remove(f)


files = []

for ft in file_types:
    delete_files(glob.glob(ft))
